package com.everis.examen.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Class Company.
 */
@Document
public class Company {

	/** The street. */
	@Field
	public String name;

	/** The suite. */
	@Field
	public String catchPhrase;

	/** The city. */
	@Field
	public String bs;

	/**
	 * Instantiates a new company.
	 */
	public Company() {
	}

	/**
	 * Instantiates a new company.
	 *
	 * @param name the name
	 * @param catchPhrase the catch phrase
	 * @param bs the bs
	 */
	public Company(String name, String catchPhrase, String bs) {
		super();
		this.name = name;
		this.catchPhrase = catchPhrase;
		this.bs = bs;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Company [name=" + name + ", catchPhrase=" + catchPhrase + ", bs=" + bs + "]";
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name.
	 *
	 * @param name the new name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the catch phrase.
	 *
	 * @return the catch phrase
	 */
	public String getCatchPhrase() {
		return catchPhrase;
	}

	/**
	 * Sets the catch phrase.
	 *
	 * @param catchPhrase the new catch phrase
	 */
	public void setCatchPhrase(String catchPhrase) {
		this.catchPhrase = catchPhrase;
	}

	/**
	 * Gets the bs.
	 *
	 * @return the bs
	 */
	public String getBs() {
		return bs;
	}

	/**
	 * Sets the bs.
	 *
	 * @param bs the new bs
	 */
	public void setBs(String bs) {
		this.bs = bs;
	}

}
