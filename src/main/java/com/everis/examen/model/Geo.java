package com.everis.examen.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Class Geo.
 */
@Document
public class Geo {

	/** The lat. */
	@Field
	public String lat;

	/** The lng. */
	@Field
	public String lng;

	/**
	 * Instantiates a new geo.
	 */
	public Geo() {
	}

	/**
	 * Instantiates a new geo.
	 *
	 * @param lat the lat
	 * @param lng the lng
	 */
	public Geo(String lat, String lng) {
		super();
		this.lat = lat;
		this.lng = lng;
	}

	/**
	 * Gets the lat.
	 *
	 * @return the lat
	 */
	public String getLat() {
		return lat;
	}

	/**
	 * Sets the lat.
	 *
	 * @param lat the new lat
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}

	/**
	 * Gets the lng.
	 *
	 * @return the lng
	 */
	public String getLng() {
		return lng;
	}

	/**
	 * Sets the lng.
	 *
	 * @param lng the new lng
	 */
	public void setLng(String lng) {
		this.lng = lng;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Geo [lat=" + lat + ", lng=" + lng + "]";
	}

}
