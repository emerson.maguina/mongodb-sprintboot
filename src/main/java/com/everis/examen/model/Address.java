package com.everis.examen.model;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * The Class Address.
 */
@Document
public class Address {

	/** The street. */
	@Field
	public String street;

	/** The suite. */
	@Field
	public String suite;

	/** The city. */
	@Field
	public String city;

	/** The zipcode. */
	@Field
	public String zipcode;

	/** The geo. */
	@Field
	public Geo geo;

	/**
	 * Instantiates a new address.
	 */
	public Address() {
	}

	/**
	 * Instantiates a new address.
	 *
	 * @param street  the street
	 * @param suite   the suite
	 * @param city    the city
	 * @param zipcode the zipcode
	 * @param geo     the geo
	 */
	public Address(String street, String suite, String city, String zipcode, Geo geo) {
		super();
		this.street = street;
		this.suite = suite;
		this.city = city;
		this.zipcode = zipcode;
		this.geo = geo;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		return "Address [street=" + street + ", suite=" + suite + ", city=" + city + ", zipcode=" + zipcode + ", geo="
				+ geo + "]";
	}

	/**
	 * Gets the street.
	 *
	 * @return the street
	 */
	public String getStreet() {
		return street;
	}

	/**
	 * Sets the street.
	 *
	 * @param street the new street
	 */
	public void setStreet(String street) {
		this.street = street;
	}

	/**
	 * Gets the suite.
	 *
	 * @return the suite
	 */
	public String getSuite() {
		return suite;
	}

	/**
	 * Sets the suite.
	 *
	 * @param suite the new suite
	 */
	public void setSuite(String suite) {
		this.suite = suite;
	}

	/**
	 * Gets the city.
	 *
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * Sets the city.
	 *
	 * @param city the new city
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * Gets the zipcode.
	 *
	 * @return the zipcode
	 */
	public String getZipcode() {
		return zipcode;
	}

	/**
	 * Sets the zipcode.
	 *
	 * @param zipcode the new zipcode
	 */
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	/**
	 * Gets the geo.
	 *
	 * @return the geo
	 */
	public Geo getGeo() {
		return geo;
	}

	/**
	 * Sets the geo.
	 *
	 * @param geo the new geo
	 */
	public void setGeo(Geo geo) {
		this.geo = geo;
	}

}
