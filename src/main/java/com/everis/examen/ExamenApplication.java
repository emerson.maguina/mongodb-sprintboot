package com.everis.examen;

import java.util.List;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.GroupOperation;
import org.springframework.data.mongodb.core.aggregation.MatchOperation;
import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
import org.springframework.data.mongodb.core.aggregation.SortOperation;
import org.springframework.data.mongodb.core.query.Criteria;

import com.everis.examen.dto.UserLenghtName;
import com.everis.examen.dto.UserReport;
import com.everis.examen.model.User;
import com.everis.examen.repository.UserRepository;
import com.everis.examen.util.UtilLoad;

@SpringBootApplication
public class ExamenApplication {

	public static void main(String[] args) {
		SpringApplication.run(ExamenApplication.class, args);
	}

	@Bean
	public CommandLineRunner insert(UserRepository userRepository) {
		return (args) -> {

			UtilLoad util = new UtilLoad();
			List<User> users = util.loadUsers("json/users.json");
			users.stream().forEach(u -> userRepository.save(u));
			System.out.println("Insert:" + users.size() + " users.");

		};
	}

	@Bean
	public CommandLineRunner aggregationFirst(UserRepository userRepository, MongoTemplate mongoTemplate) {
		return (args) -> {

			var matchCriteria = new Criteria("address.city").is("McKenziehaven");
			MatchOperation match = Aggregation.match(matchCriteria);
			var sortByCount = Aggregation.sortByCount("address.street");
			ProjectionOperation projection = Aggregation.project().and("_id").as("name").and("count").as("amount");
			var sortByCountAgregation = Aggregation.newAggregation(match, sortByCount, projection);
			var result = mongoTemplate.aggregate(sortByCountAgregation, "user", UserReport.class);
			var user = result.getMappedResults();
			user.stream().forEach(u -> mongoTemplate.save(u, "userCount"));
			user.stream().forEach(u -> {
				System.out.println(u.name);
				System.out.println(u.amount);
			});

		};
	}
	
	@Bean
	public CommandLineRunner mongoExportAggreationFirts(UserRepository userRepository, MongoTemplate mongoTemplate) {
		return (args) -> {
			Runtime.getRuntime().exec(setCommandRuntime("mongoexport", "newHorizontEveris", "userCount", "localhost",
					"27022", "json/usercount.json"));
		};
	}

	@Bean
	public CommandLineRunner aggregationSecond(UserRepository userRepository, MongoTemplate mongoTemplate) {
		return (args) -> {

			ProjectionOperation projection = Aggregation.project().andExclude("_id").and("username").as("username")
					.andExpression("strLenCP(username)").as("maxcharlength");
			GroupOperation group = Aggregation.group("maxcharlength").count().as("count");
			SortOperation sort = Aggregation.sort(Sort.by(Direction.DESC, "count"));
			ProjectionOperation projection2 = Aggregation.project().and("_id").as("maxcharlength").and("count").as("total");
			var groupSortAgregation = Aggregation.newAggregation(projection, group, sort, projection2);
			var result = mongoTemplate.aggregate(groupSortAgregation, "user", UserLenghtName.class);
			var user = result.getMappedResults();
			user.stream().forEach(u -> mongoTemplate.save(u, "userLenghtName"));
			user.stream().forEach(u -> {
				System.out.println(u.maxcharlength);
				System.out.println(u.total);
			});

		};
	}

	@Bean
	public CommandLineRunner mongoExportAggreationSecond(UserRepository userRepository, MongoTemplate mongoTemplate) {
		return (args) -> {
			Runtime.getRuntime().exec(setCommandRuntime("mongoexport", "newHorizontEveris", "userLenghtName", "localhost",
					"27022", "json/userlenghtname.json"));
		};
	}

	private String setCommandRuntime(String command, String db, String col, String host, String port, String fileName) {
		return command + " --host " + host + " --port " + port + " --db " + db + " --collection " + col + " --out "
				+ fileName + " --authenticationDatabase admin ";
	}
}
