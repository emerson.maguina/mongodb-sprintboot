package com.everis.examen.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.List;

import com.everis.examen.model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class UtilLoad {
	
	/** The class loader. */
    ClassLoader classLoader = getClass().getClassLoader();

    /** The gson. */
    Gson gson = new Gson();
    
    /**
     * Load users.
     * @param jsonPath the json path
     * @return the list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("serial")
    public List<User> loadUsers(String jsonPath) throws IOException {
        File file = new File(this.classLoader.getResource(jsonPath).getFile());
        String content = new String(Files.readAllBytes(file.toPath()));
        return this.gson.fromJson(content, new TypeToken<List<User>>() {
        }.getType());
    }

}
